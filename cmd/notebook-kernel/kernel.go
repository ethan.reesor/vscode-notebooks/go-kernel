package main

import (
	"context"
	"fmt"
	"runtime/debug"

	"github.com/traefik/yaegi/stdlib"
	"gitlab.com/ethan.reesor/vscode-notebooks/go-kernel/src/proto"
	"gitlab.com/ethan.reesor/vscode-notebooks/lib/kernelpb"
	"gitlab.com/ethan.reesor/vscode-notebooks/lib/notebook"
	"gitlab.com/ethan.reesor/vscode-notebooks/lib/yaegi"
)

type kernel struct {
	proto.UnsafeKernelServer
}

type session struct {
	stream proto.Kernel_SessionServer
	interp *yaegi.Session

	cached   chan *kernelpb.VSCode_Cached
	prompted chan *kernelpb.VSCode_Prompted
}

func (k *kernel) Session(stream proto.Kernel_SessionServer) error {
	s := &session{
		stream:   stream,
		interp:   yaegi.NewSession(stream.Context()),
		cached:   make(chan *kernelpb.VSCode_Cached),
		prompted: make(chan *kernelpb.VSCode_Prompted),
	}

	if err := s.interp.Use(stdlib.Symbols); err != nil {
		return fmt.Errorf("failed to import stdlib: %v", err)
	}
	if err := s.UseNotebookAPI(); err != nil {
		return fmt.Errorf("failed to import notebook api: %v", err)
	}
	if err := s.UseVSCodeAPI(); err != nil {
		return fmt.Errorf("failed to import notebook api: %v", err)
	}
	s.interp.ImportUsed()

	evalSem := make(chan struct{}, 1)

	var cancel context.CancelFunc = func() {}
	for {
		msg, err := stream.Recv()
		if err != nil {
			return err
		}

		switch v := msg.Get().(type) {
		case *kernelpb.Eval_Code:
			var ctx context.Context
			ctx, cancel = context.WithCancel(stream.Context())

			evalSem <- struct{}{}
			go func() {
				defer cancel()
				defer func() { <-evalSem }()

				var result kernelpb.Eval_Result
				defer stream.Send(&proto.FromKernel{Kind: &proto.FromKernel_Evaluated{Evaluated: &result}})

				_, d, err := s.interp.Eval(ctx, v.Code, yaegi.EvalOptions{
					OnStdout: func(b []byte) { s.SendOutput(notebook.Content{Type: "stdout", Value: b}) },
					OnStderr: func(b []byte) { s.SendOutput(notebook.Content{Type: "stderr", Value: b}) },
					OnPanic:  func(v interface{}) { result.AddError(fmt.Sprint(v), string(debug.Stack())) },
				})
				result.SetDuration(d)

				if err != nil {
					yaegi.UnfoldError(err, &result)
				}
			}()

		case *kernelpb.Eval_Cancel:
			cancel()

		case *kernelpb.VSCode_Cached:
			s.cached <- v

		case *kernelpb.VSCode_Prompted:
			s.prompted <- v
		}
	}
}
