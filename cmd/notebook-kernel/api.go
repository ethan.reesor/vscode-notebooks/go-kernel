package main

import (
	"gitlab.com/ethan.reesor/vscode-notebooks/go-kernel/src/proto"
	"gitlab.com/ethan.reesor/vscode-notebooks/lib/kernelpb"
	"gitlab.com/ethan.reesor/vscode-notebooks/lib/notebook"
	"gitlab.com/ethan.reesor/vscode-notebooks/lib/yaegi"
)

type vscodeInterface struct {
	*session
}

func (i vscodeInterface) Done() <-chan struct{} {
	return i.stream.Context().Done()
}

func (i vscodeInterface) Prompted() <-chan *kernelpb.VSCode_Prompted {
	return i.prompted
}

func (i vscodeInterface) Cached() <-chan *kernelpb.VSCode_Cached {
	return i.cached
}

func (i vscodeInterface) Prompt(msg *kernelpb.VSCode_Prompt) {
	i.stream.Send(&proto.FromKernel{
		Kind: &proto.FromKernel_Prompt{
			Prompt: msg,
		},
	})
}

func (i vscodeInterface) ReadCache(msg *kernelpb.VSCode_ReadCache) {
	i.stream.Send(&proto.FromKernel{
		Kind: &proto.FromKernel_ReadCache{
			ReadCache: msg,
		},
	})
}

func (i vscodeInterface) WriteCache(msg *kernelpb.VSCode_WriteCache) {
	i.stream.Send(&proto.FromKernel{
		Kind: &proto.FromKernel_WriteCache{
			WriteCache: msg,
		},
	})
}

func (s *session) SendOutput(content ...notebook.Content) error {
	var msg proto.FromKernel
	msg.Output(content...)
	return s.stream.Send(&msg)
}

func (s *session) UseVSCodeAPI() error {
	return yaegi.UseVSCodeAPI(s.interp, vscodeInterface{s})
}

func (s *session) UseNotebookAPI() error {
	return yaegi.NotebookAPI(s.interp, func(o notebook.Formatter) {
		v, err := o.Format()
		if err != nil {
			s.SendOutput(notebook.Content{Type: "stderr", Value: []byte(err.Error())})
			return
		}
		s.SendOutput(v...)
	}).Export()
}
