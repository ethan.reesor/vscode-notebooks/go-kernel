package proto

import (
	"gitlab.com/ethan.reesor/vscode-notebooks/lib/kernelpb"
	"gitlab.com/ethan.reesor/vscode-notebooks/lib/notebook"
)

//go:generate protoc -I../.. --go_out=../.. --go_opt=paths=source_relative --go-grpc_out=../.. --go-grpc_opt=paths=source_relative src/proto/kernel.proto

func String(v string) *string {
	if v == "" {
		return nil
	}
	return &v
}

func (msg *ToKernel) Get() interface{} {
	switch v := msg.Kind.(type) {
	case *ToKernel_Evaluate:
		return v.Evaluate
	case *ToKernel_CancelEval:
		return v.CancelEval
	case *ToKernel_Cached:
		return v.Cached
	case *ToKernel_Prompted:
		return v.Prompted
	default:
		return v
	}
}

func (msg *FromKernel) Output(output ...notebook.Content) {
	content := make([]*kernelpb.Content, len(output))
	for i, o := range output {
		content[i] = &kernelpb.Content{Type: o.Type, Value: o.Value}
	}

	msg.Kind = &FromKernel_Output{
		Output: &kernelpb.Eval_Output{
			Content: content,
		},
	}
}
