// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('@grpc/grpc-js');
var src_proto_kernel_pb = require('../../src/proto/kernel_pb.js');
var shared_kernel_pb = require('../../shared/kernel_pb.js');

function serialize_FromKernel(arg) {
  if (!(arg instanceof src_proto_kernel_pb.FromKernel)) {
    throw new Error('Expected argument of type FromKernel');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_FromKernel(buffer_arg) {
  return src_proto_kernel_pb.FromKernel.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_ToKernel(arg) {
  if (!(arg instanceof src_proto_kernel_pb.ToKernel)) {
    throw new Error('Expected argument of type ToKernel');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_ToKernel(buffer_arg) {
  return src_proto_kernel_pb.ToKernel.deserializeBinary(new Uint8Array(buffer_arg));
}


var KernelService = exports.KernelService = {
  session: {
    path: '/Kernel/Session',
    requestStream: true,
    responseStream: true,
    requestType: src_proto_kernel_pb.ToKernel,
    responseType: src_proto_kernel_pb.FromKernel,
    requestSerialize: serialize_ToKernel,
    requestDeserialize: deserialize_ToKernel,
    responseSerialize: serialize_FromKernel,
    responseDeserialize: deserialize_FromKernel,
  },
};

exports.KernelClient = grpc.makeGenericClientConstructor(KernelService);
