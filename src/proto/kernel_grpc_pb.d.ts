// package: 
// file: src/proto/kernel.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "@grpc/grpc-js";
import * as src_proto_kernel_pb from "../../src/proto/kernel_pb";
import * as shared_kernel_pb from "../../shared/kernel_pb";

interface IKernelService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    session: IKernelService_ISession;
}

interface IKernelService_ISession extends grpc.MethodDefinition<src_proto_kernel_pb.ToKernel, src_proto_kernel_pb.FromKernel> {
    path: "/Kernel/Session";
    requestStream: true;
    responseStream: true;
    requestSerialize: grpc.serialize<src_proto_kernel_pb.ToKernel>;
    requestDeserialize: grpc.deserialize<src_proto_kernel_pb.ToKernel>;
    responseSerialize: grpc.serialize<src_proto_kernel_pb.FromKernel>;
    responseDeserialize: grpc.deserialize<src_proto_kernel_pb.FromKernel>;
}

export const KernelService: IKernelService;

export interface IKernelServer extends grpc.UntypedServiceImplementation {
    session: grpc.handleBidiStreamingCall<src_proto_kernel_pb.ToKernel, src_proto_kernel_pb.FromKernel>;
}

export interface IKernelClient {
    session(): grpc.ClientDuplexStream<src_proto_kernel_pb.ToKernel, src_proto_kernel_pb.FromKernel>;
    session(options: Partial<grpc.CallOptions>): grpc.ClientDuplexStream<src_proto_kernel_pb.ToKernel, src_proto_kernel_pb.FromKernel>;
    session(metadata: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientDuplexStream<src_proto_kernel_pb.ToKernel, src_proto_kernel_pb.FromKernel>;
}

export class KernelClient extends grpc.Client implements IKernelClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: Partial<grpc.ClientOptions>);
    public session(options?: Partial<grpc.CallOptions>): grpc.ClientDuplexStream<src_proto_kernel_pb.ToKernel, src_proto_kernel_pb.FromKernel>;
    public session(metadata?: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientDuplexStream<src_proto_kernel_pb.ToKernel, src_proto_kernel_pb.FromKernel>;
}
