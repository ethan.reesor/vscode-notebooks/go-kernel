// package: 
// file: src/proto/kernel.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";
import * as shared_kernel_pb from "../../shared/kernel_pb";

export class ToKernel extends jspb.Message { 

    hasEvaluate(): boolean;
    clearEvaluate(): void;
    getEvaluate(): shared_kernel_pb.Eval.Code | undefined;
    setEvaluate(value?: shared_kernel_pb.Eval.Code): ToKernel;

    hasPrompted(): boolean;
    clearPrompted(): void;
    getPrompted(): shared_kernel_pb.VSCode.Prompted | undefined;
    setPrompted(value?: shared_kernel_pb.VSCode.Prompted): ToKernel;

    hasCached(): boolean;
    clearCached(): void;
    getCached(): shared_kernel_pb.VSCode.Cached | undefined;
    setCached(value?: shared_kernel_pb.VSCode.Cached): ToKernel;

    hasCanceleval(): boolean;
    clearCanceleval(): void;
    getCanceleval(): shared_kernel_pb.Eval.Cancel | undefined;
    setCanceleval(value?: shared_kernel_pb.Eval.Cancel): ToKernel;

    getKindCase(): ToKernel.KindCase;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ToKernel.AsObject;
    static toObject(includeInstance: boolean, msg: ToKernel): ToKernel.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ToKernel, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ToKernel;
    static deserializeBinaryFromReader(message: ToKernel, reader: jspb.BinaryReader): ToKernel;
}

export namespace ToKernel {
    export type AsObject = {
        evaluate?: shared_kernel_pb.Eval.Code.AsObject,
        prompted?: shared_kernel_pb.VSCode.Prompted.AsObject,
        cached?: shared_kernel_pb.VSCode.Cached.AsObject,
        canceleval?: shared_kernel_pb.Eval.Cancel.AsObject,
    }

    export enum KindCase {
        KIND_NOT_SET = 0,
        EVALUATE = 1,
        PROMPTED = 2,
        CACHED = 3,
        CANCELEVAL = 4,
    }

}

export class FromKernel extends jspb.Message { 

    hasOutput(): boolean;
    clearOutput(): void;
    getOutput(): shared_kernel_pb.Eval.Output | undefined;
    setOutput(value?: shared_kernel_pb.Eval.Output): FromKernel;

    hasEvaluated(): boolean;
    clearEvaluated(): void;
    getEvaluated(): shared_kernel_pb.Eval.Result | undefined;
    setEvaluated(value?: shared_kernel_pb.Eval.Result): FromKernel;

    hasReadcache(): boolean;
    clearReadcache(): void;
    getReadcache(): shared_kernel_pb.VSCode.ReadCache | undefined;
    setReadcache(value?: shared_kernel_pb.VSCode.ReadCache): FromKernel;

    hasWritecache(): boolean;
    clearWritecache(): void;
    getWritecache(): shared_kernel_pb.VSCode.WriteCache | undefined;
    setWritecache(value?: shared_kernel_pb.VSCode.WriteCache): FromKernel;

    hasPrompt(): boolean;
    clearPrompt(): void;
    getPrompt(): shared_kernel_pb.VSCode.Prompt | undefined;
    setPrompt(value?: shared_kernel_pb.VSCode.Prompt): FromKernel;

    getKindCase(): FromKernel.KindCase;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): FromKernel.AsObject;
    static toObject(includeInstance: boolean, msg: FromKernel): FromKernel.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: FromKernel, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): FromKernel;
    static deserializeBinaryFromReader(message: FromKernel, reader: jspb.BinaryReader): FromKernel;
}

export namespace FromKernel {
    export type AsObject = {
        output?: shared_kernel_pb.Eval.Output.AsObject,
        evaluated?: shared_kernel_pb.Eval.Result.AsObject,
        readcache?: shared_kernel_pb.VSCode.ReadCache.AsObject,
        writecache?: shared_kernel_pb.VSCode.WriteCache.AsObject,
        prompt?: shared_kernel_pb.VSCode.Prompt.AsObject,
    }

    export enum KindCase {
        KIND_NOT_SET = 0,
        OUTPUT = 1,
        EVALUATED = 2,
        READCACHE = 3,
        WRITECACHE = 4,
        PROMPT = 5,
    }

}
