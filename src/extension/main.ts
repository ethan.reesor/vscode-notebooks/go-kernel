import * as vscode from 'vscode';
import { Kernel } from './kernel';

const kernel = new Kernel()

export async function activate(context: vscode.ExtensionContext) {
	for (const type of ['markdown-notebook', 'interactive']) {
		const controller = vscode.notebooks.createNotebookController(`go-kernel-${type}`, type, 'Go Kernel')
		controller.supportedLanguages = ['go']
		controller.executeHandler = (cells, doc, ctrl) => kernel.executeCells(doc, cells, ctrl)
		controller.interruptHandler = doc => kernel.interrupt(doc)
	}

	context.subscriptions.push(vscode.commands.registerCommand('goNotebookKernel.kernel.restart', () => {
		kernel.kill('SIGTERM')
	}))

	context.subscriptions.push(vscode.commands.registerCommand('goNotebookKernel.kernel.rebuild', async () => {
		await Kernel.install(false)
	}))

	context.subscriptions.push(vscode.commands.registerCommand('goNotebookKernel.kernel.stopSession', async () => {
		// requires vscode.window.activeNotebookEditor
		throw new Error(`not implemented`)
		// const uri = vscode.window.activeTextEditor?.document?.uri
		// if (uri) kernel.stopSession(uri)
	}))

	context.subscriptions.push(vscode.commands.registerCommand('goNotebookKernel.openInteractive', async () => {
		await vscode.commands.executeCommand('interactive.open', vscode.ViewColumn.Active, void 0, 'go-kernel-interactive')
	}))
}

export function deactivate() {
	kernel.kill('SIGKILL')
}