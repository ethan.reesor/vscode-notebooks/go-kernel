# Go Notebooks for VSCode

> Powered by [Yaegi](https://github.com/traefik/yaegi)

A notebook extension for VSCode that uses Yaegi for evaluating Go.

![hello world](images/hello-world.png)

## Compatible formats

Theoretically, the Go kernel should work with any notebook format. Currently, it
is only registered for [Markdown
Notebooks](https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-markdown-notebook).

## Interactive window

You can open an interactive Go window with the command `Go Notebook Kernel: Open
Interactive`.

## API

The interpreter exposes VSCode functionality through 'core' packages.

![api](images/api.png)

```go
package notebook

import nb "gitlab.com/ethan.reesor/vscode-notebooks/lib/notebook"

type (
	// A Formatter can be converted into an array of content pairs for use as a
	// notebook output cell.
	Formatter = nb.Formatter

	// Content is a (mime type, content) pair used as a notebook output cell.
	Content = nb.Content

	// A MultiFormat groups multiple formatters together.
	MultiFormat = nb.MultiFormat

	// SpewFormat formats a value with github.com/davecgh/go-spew.
	SpewFormat = nb.SpewFormat

	// SQLFormat formats *sql.Rows as JSON, CSV, and plain text.
	SQLFormat = nb.SQLFormat

	// JSONFormat formats a value as JSON. Values other than string and []byte
	// are marshalled.
	JSONFormat = nb.JSONFormat

	// XMLFormat formats a value as XML. Values other than string and []byte
	// are marshalled.
	XMLFormat = nb.XMLFormat

	// MarkdownFormat formats a value as Markdown. If the language is not blank,
	// the output will be surrounded with code fences. If the language is json
	// or xml and the value is not string or []byte, the value is marshalled.
	MarkdownFormat = nb.MarkdownFormat
)

// Halt immediately stops evaluation.
func Halt(interface{})

// Inspect dumps detailed information on each value, using SpewFormat.
func Inspect(...interface{})

// Show creates an output cell for each value. `*sql.Rows` values are formatted
// with SQLFormat (as JSON, HTML, and CSV). Values that implement Formatter are
// formatted with Formatter.Format(). Other values are formatted with
// SpewFormat.
func Show(...interface{})
```

```go
package vscode

// PromptOptions are options for Prompt.
type PromptOptions struct {
	// Prompt is the message prompt.
	Prompt      string

	// Placeholder is shown as a placeholder in the input box.
	Placeholder string

	// Password sets whether the input should be treated as a password.
	Password    bool
}

// Prompt opens a VSCode prompt. Prompt returns the value entered and whether
// the prompt was aborted.
func Prompt(*PromptOptions) (v string, ok bool)

// Cache caches values in VSCode's memory. The first time Cache is called for a
// given key, get will be called. As long as VSCode remains running, subsequent
// calls to Cache using the same key will return the same value without calling
// get.
func Cache(key string, get func() string) string

// Uncache deletes cached values.
func Uncache(key string)
```