module gitlab.com/ethan.reesor/vscode-notebooks/go-kernel

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/denisenkom/go-mssqldb v0.9.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/protobuf v1.5.1 // indirect
	github.com/lib/pq v1.10.0
	github.com/traefik/yaegi v0.9.21
	gitlab.com/ethan.reesor/vscode-notebooks/lib v0.0.0-20210810051416-754b8fa3ad4e
	google.golang.org/grpc v1.36.0
	google.golang.org/protobuf v1.27.1
)
